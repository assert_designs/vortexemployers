<?php

require_once 'vendor/autoload.php';

use Illuminate\Database\Capsule\Manager as Capsule;
use App\Models\Employer;

$capsule = new Capsule;

$capsule->addConnection([
    'driver'    => 'mysql',
    'host'      => 'localhost',
    'database'  => 'employers_vortex',
    'username'  => 'root',
    'password'  => '',
    'charset'   => 'utf8',
    'collation' => 'utf8_unicode_ci',
    'prefix'    => '',
]);

// Make this Capsule instance available globally via static methods... (optional)
$capsule->setAsGlobal();
// Setup the Eloquent ORM... (optional; unless you've used setEventDispatcher())
$capsule->bootEloquent();

if (!empty($_POST)) {
    $employer = new Employer();
    $employer->id = $_POST['id'];
    $employer->names = $_POST['names'].' '.$_POST['lastnames'];
    $employer->typeId = $_POST['types'][0];
    $employer->one_number = $_POST['one_number'];
    $employer->two_number = $_POST['two_number'];
    $employer->three_number = $_POST['three_number'];
    $employer->email = $_POST['email'];
    $employer->salary = $_POST['salary'];
    $employer->save();
}


?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Add Employer | Employers</title>

    <link rel="stylesheet" href="css/employer_stl.css" type="text/css">

    <!-- Compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">

    <!-- Compiled and minified JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
</head>

<body id="as">
    <header>
        <div id="top-nav"></div>
        <div class="main-nav-desktop"></div>
    </header>
    <main>
        <form id="main-form" action="AddEmployer.php" method="post">
            <div id="main-form-content">
                <h2>Registrar Empleado</h2>

                <input class="fields" type="text" name="names" id="names" placeholder="Ingresa tus nombres">
                <input class="fields" type="text" name="lastnames" id="lastnames" placeholder="Ingresa tus apellidos">
                <input class="fields" type="text" name="id" id="number-id"
                    placeholder="Ingresa tu número de identificación">
                <p id="title-type">Escoge tu tipo de identificación:</p>
                <p>
                    <label>
                        <input class="filled-in" type="checkbox" name="types[]" value="CC" />
                        <span>CC</span>
                    </label>
                </p>
                <p>
                    <label>
                        <input class="filled-in" type="checkbox" name="types[]" value="CE" />
                        <span>CE</span>
                    </label>
                </p>
                <input class="fields" type="text" name="one_number" id="number-one"
                    placeholder="Ingresa un número de télefono">
                <input class="fields" type="text" name="two_number" id="number-two"
                    placeholder="Ingresa un número de télefono">
                <input class="fields" type="text" name="three_number" id="number-three"
                    placeholder="Ingresa un número de télefono">
                <input class="fields" type="text" name="email" id="email" placeholder="Ingresa un correo eléctronico">
                <input class="fields" type="text" name="salary" id="salary" placeholder="Ingresa tu salario">
                <div id="main-btns">
                    <input class="btn" type="submit" value="Registrar Empleado">
                </div>
            </div>
        </form>
    </main>
</body>

</html>