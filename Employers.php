<?php 

require_once 'vendor/autoload.php';

use App\Models\Employer;

$employers = Employer::all();

function printEmployers($employer) {

    echo '<div class="row employer">';
    echo '<div id="field-title" class="col s12 m10 l1">';
    echo '<h3>';
    echo '<label>';
    echo '<input type="checkbox" class="filled-in"/>';
    echo '<span id="id-ct">'.$employer->id.'</span>';
    echo '</label>';
    echo '</h3>';
    echo '</div>';
    echo '<div id="field-title" class="col s12 m10 l2"><h3>'.$employer->names.'</h3></div>';
    echo '<div id="field-title" class="col s12 m10 l1"><h3>'.$employer->typeId.'</h3></div>';
    echo '<div id="field-title" class="col s12 m10 l2"><h3>'.$employer->one_number.'</h3></div>';
    echo '<div id="field-title" class="col s12 m10 l2"><h3>'.$employer->email.'</h3></div>';
    echo '<div id="field-title" class="col s12 m10 l2"><h3>'.$employer->salary.'</h3></div>';
    echo '<div id="field-title" class="col s12 m10 l2"><h3>Acciones</h3></div>';
    echo '</div>';
    echo '<div class="separator"></div>';

}