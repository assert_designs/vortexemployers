<?php

namespace App\Models;

interface PrintableBE {

    public function getPhones();

    public function getEmail();

    public function getSalary();

}