<?php

namespace App\Models;

interface PrintableEmployer {

    public function getIdEmployer();

    public function getNames();

    public function getTypeId();

}